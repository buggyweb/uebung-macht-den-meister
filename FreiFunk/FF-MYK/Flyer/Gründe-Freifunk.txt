

Freifunk – Internet-Zugang für Smartphones, Tablets, Notebooks

Der Zugang zum Internet über Funk ist für uns zur alltäglichen Selbstverständlichkeit geworden. Freifunk-Dreiländereck ist eine vom Freifunk Dreiländereck e.V. betriebene Initiative, um einen offenen, schnellen und kostenlosen Internet-Zugang flächendeckend in der gesamten Regio zur Verfügung zu stellen –  und das ohne große Investitionen.

 

Freifunk – was ist das? Was nutzt er mir? Wie kann ich ihn nutzen?

Wie verbinden sich Smartphones, Tablets, Notebooks über Funk mit dem Internet?

    Zu Hause funktioniert das mittels eines schnellen Funknetzes, häufig als „WLAN“ bezeichnet. Über dieses WLAN kommunizieren alle mobilen, internetfähigen Geräte, wie z. B. Personal Computer, Notebook, Tablet oder Smartphone, ohne Beschränkung der Datenmenge.
    Unterwegs nutzen die internetfähigen mobilen Geräte, in den meisten Fällen Smartphones, die Funk-Telefonverbindung. Die Übertragungsgeschwindigkeit und die Datenmenge sind dabei beschränkt und hängen vom jeweiligen Vertrag ab. Bei Überschreitung der vertraglich festgelegten Datenmenge sinkt die Übertragungsgeschwindigkeit drastisch ab bzw. muss ein zusätzliches Datenkontingent nachgekauft werden. Je höher die Geschwindigkeit und größer die Datenmenge desto teurer sind die Gebühren.

Was ist der Nutzen von Freifunk?

Freifunk bietet unterwegs über WLAN einen schnellen Zugang zum Internet ohne Beschränkung der Datenmenge – ganz wie zu Hause – und dazu kostenlos.

Wie kann ich mein mobiles Gerät mit Freifunk verbinden?

Die Nutzung von Freifunk ist denkbar einfach: Wer sich in der Regio in einem Freifunk-Sendebereich aufhält, sucht nach dem WLAN „Freifunk“, wählt dieses aus und wird direkt mit dem Internet verbunden.

 

Wer bietet Freifunk in der Regio an?

Die Freifunk-Initiative ist Teil einer weltweiten Open Wireless-Bewegung. Der Freifunk Dreiländereck e.V. ist ein gemeinnütziger Verein, der sich die flächendeckende Verbreitung von Freifunk in der Regio zum Ziel gesetzt hat. Alle sind eingeladen, mitzumachen und können einen Freifunk-Zugang anbieten, auch ohne Vereinsmitglied zu sein.

 

Freifunk – was nutzt er mir als Freifunk-Anbieter?

Gastronomie und Hotels – durch zusätzlichen Service attraktiver machen

    Als Betreiber eines gastronomischen Betriebes oder Hotels möchte ich meine Gäste zum genüsslichen Verweilen einladen und ihnen zusätzlichen Komfort bieten. Ein kostenloser, schneller Internet-Zugang per WLAN gehört heutzutage dazu.

Arztpraxen und Dienstleistungsbetriebe – Wartezeiten überbrücken

    Ich betreibe eine Arztpraxis oder einen Dienstleistungsbetrieb und will meinen Patienten bzw. Kunden die Wartezeiten nicht nur durch Zeitschriften, sondern zeitgemäß auch durch einen kostenlosen Internet-Zugang über WLAN verkürzen.

Fachgeschäfte – Attraktivität steigern im Wettbewerb mit Online-Handel

    Im zunehmenden Wettbewerb mit dem Online-Handel will ich den Einkauf und das Verweilen in meinem Fachgeschäft durch einen kostenlosen, schnellen Zugang zum Internet noch angenehmer gestalten.

Arbeitgeber – Internet für meine Mitarbeitenden

    Als moderner, innovativer Arbeitgeber möchte ich meinen Mitarbeitenden z.B. in Pausenbereichen oder im Betriebsrestaurant über WLAN Zugang zum Internet ermöglichen, ohne die IT-Sicherheit meines Unternehmens zu gefährden.

Gewerbetreibende und Industrie – Internet für Besucher

    Ich empfange täglich Besucher, die ihre Computer oder Smartphones gerne über das WLAN mit dem Internet verbinden wollen. Meine Richtlinien zur Informationssicherheit lassen dies jedoch nicht zu. Freifunk bietet die perfekte Lösung.

Behörden, Bürgerbüros – Bürgernähe und Attraktivität steigern

    Als Verantwortlicher für eine Behörde oder ein Bürgerbüro will ich den Bürgern einen kostenlosen und schnellen Internet-Zugang als eine innovative, freundliche Dienstleistung anbieten und auch auf diese Weise Bürgernähe praktizieren.

Vereine, Organisationen – Internet-Zugang für Mitglieder

    Als Vorstand eines Vereins oder einer Organisation will ich den Mitgliedern in unseren Räumlichkeiten einen zeitgemäßen Zugang zum Internet zur Verfügung stellen. Das unterstreicht unsere innovative Grundeinstellung.

Innovative, weltoffene Bürger – aktiver Beitrag zum flächendeckenden Internet

    Ich weiß, wie wichtig für Deutschland und die anderen europäischen Länder  Weltoffenheit, Innovation und Standort-Attraktivität sind. Daher will ich zur innovativen Idee eines flächendeckenden, schnellen und kostenlosen Internet-Zugangs per WLAN aktiv beitragen. Freifunk ist dazu ein wichtiger Schritt, den ich selbst mit geringem finanziellen Aufwand vollziehen kann.

 

Was muss ich tun, um Freifunk anbieten zu können?

Ein Internet-Zugang sollte bereits vorhanden sein. Dann braucht es keine große Investition, sondern lediglich die schnell und einfach zu bewerkstelligende Installation eines zusätzlichen Gerätes namens Freifunk-Router. Einfache Ausführungen, die für die meisten Anwendungsfälle ausreichen, kosten einmalig etwa 20 € bzw. 25 CHF. Also recht überschaubare Kosten als Beitrag für sehr viel Nutzen.

Wer einen Freifunk-Zugangspunkt, also einen Freifunk-Router, bei sich installiert, gewährt anderen Menschen kostenlos die Nutzung eines Teils der Übertragungskapazität des privaten Internet-Anschlusses. Dies wird nur in besonderen Ausnahmefällen zu spürbaren Einschränkungen führen. Da ein Freifunk-Anbieter die vollständige Kontrolle über den Freifunk-Router hat, lassen sich derartige Fälle über entsprechende Einstellungen leicht beherrschen.

 

Welche Haftungsrisiken gehe ich mit Freifunk ein?

Kurz gesagt, keine. Die sogenannte Störerhaftung betrifft diejenigen nicht, die einen Internet-Zugang über Freifunk bereitstellen. Der Grund: Der Freifunk Dreiländereck e.V. ist selbst ein Internet Service Provider, wie z.B. die Telekom, und als solcher befreit von der Störerhaftung. Durch die indirekte Weiterleitung des Datenverkehrs durch das Freifunk-Netz erfolgt der Zugang zum Internet anonym.

 

Welche Sicherheitsrisiken gehe ich mit Freifunk ein?

Auch diese Frage lässt sich knapp beantworten: keine. Freifunk nutzt eine weltweit bewährte Technologie, VPN, die den Datenverkehr zwischen dem Freifunk-Router und dem Internet-Anschlusspunkt des Freifunk-Vereins verschlüsselt durchleitet.

 

Ich bin begeistert von Freifunk – wer hilft mir, Freifunker zu werden?

Um bei Freifunk Dreiländereck mitmachen zu können, braucht es keine besonderen Vorkenntnisse. Der Verein Freifunk Dreiländereck e.V. hilft gerne dabei.

 
